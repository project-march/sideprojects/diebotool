# Project MARCH Diebotool
The diebotool is a simple shell tool that can read out the values taht the iebo from the MARCCH exoskeleton sends over the ethercat.
For this tool te work we rely on the SOEM code, shich is also embedded in this repo.
To find more on the SOEM code please go to: https://github.com/OpenEtherCATsociety/SOEM

BUILDING
========


Prerequisites
-------------------------------

 * CMake 3.9 or later

Linux
--------------

   * `mkdir build`
   * `cd build`
   * `cmake ..`
   * `make`


Running
=======

To run the diebotool, open a shell, and enter he following:
go into the root folder of the repo and type the following:

`sudo build/diebotool ${ethercat_port}`

Where ${ethercat_port}  is the port where the ethercat train of the exo is connected with the master.
