/** \file
 * \brief Example code for Simple Open EtherCAT master
 *
 * Usage : simple_test [ifname1]
 * ifname is NIC interface, f.e. eth0
 *
 * This is a minimal test.
 *
 * (c)Arthur Ketels 2010 - 2011
 */

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include "ethercat.h"
#include <unistd.h>
#include <signal.h>

#define EC_TIMEOUTMON 500

char IOmap[4096];
OSAL_THREAD_HANDLE thread1;
int expectedWKC;
boolean needlf;
volatile int wkc;
boolean volatile keepRunning = TRUE;
boolean inOP;
uint8 currentgroup = 0;
int selected_joints[8];
int num_selected_joints = 0;
int num_values_to_print = 0;

boolean printAbsolutePosition = FALSE;
boolean printRelativePosition = FALSE;
boolean printDieBoSlaveError = FALSE;
boolean printEncoderError = FALSE;
boolean printMotorError = FALSE;
boolean printOdriveState = FALSE;
boolean printOdriveError = FALSE;

const char* joints[] = {
	"left_ankle",
	"left_hip_aa",
	"left_hip_fe",
	"left_knee",
	"right_ankle",
	"right_hip_aa",
	"right_hip_fe",
	"right_knee",
};

const int joint_slave_indices[] = {
	3,
	4,
	4,
	3,
	2,
	1,
	1,
	2,
};

typedef struct InputFrame
{
	uint32_t Axis0AbsolutePosition;
	float Axis0Current;
	float Axis0MotorVelocity;
	uint32_t Axis0Error;
	uint32_t Axis0MotorError;
	uint32_t Axis0DieBoError;
	uint32_t Axis0EncoderError;
	uint32_t Axis0ControllerError;
	uint32_t Axis0State;
	float Axis0ODriveTemperature;
	float Axis0MotorTemperature;
	int32_t Axis0ShadowCount;
	uint32_t Axis1AbsolutePosition;
	float Axis1Current;
	float Axis1MotorVelocity;
	uint32_t Axis1Error;
	uint32_t Axis1MotorError;
	uint32_t Axis1DieBoError;
	uint32_t Axis1EncoderError;
	uint32_t Axis1ControllerError;
	uint32_t Axis1State;
	float Axis1ODriveTemperature;
	float Axis1MotorTemperature;
	int32_t Axis1ShadowCount;
	uint32_t ODriveError;
} InputFrame;

void simpletest(char *ifname)
{
	printf("\033[?25l\n\n");

    int i, oloop, iloop, chk;
    needlf = FALSE;
    inOP = FALSE;

   /* initialise SOEM, bind socket to ifname */
   if (ec_init(ifname))
   {
      /* find and auto-config slaves */

		if ( ec_config_init(FALSE) > 0 ) {

			ec_config_map(&IOmap);
			ec_configdc();

			/* wait for all slaves to reach SAFE_OP state */
			ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);

			oloop = ec_slave[0].Obytes;
			if ((oloop == 0) && (ec_slave[0].Obits > 0)) oloop = 1;
			iloop = ec_slave[0].Ibytes;
			if ((iloop == 0) && (ec_slave[0].Ibits > 0)) iloop = 1;

			expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
			ec_slave[0].state = EC_STATE_OPERATIONAL;

			/* send one valid process data to make outputs in slaves happy*/
			ec_send_processdata();
			ec_receive_processdata(EC_TIMEOUTRET);

			/* request OP state for all slaves */
			ec_writestate(0);
			chk = 200;

			/* wait for all slaves to reach OP state */
			do {
				ec_send_processdata();
				ec_receive_processdata(EC_TIMEOUTRET);
				ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
			} while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));

			if (ec_slave[0].state == EC_STATE_OPERATIONAL ) {

				inOP = TRUE;
				printf("\033[1m");
				printf("Joint\t\t");
				if (printAbsolutePosition)
					printf("Absolute pos\t");
				if (printRelativePosition)
					printf("Relative pos\t");
				if (printDieBoSlaveError)
					printf("DieBo error\t");
				if (printEncoderError)
					printf("Encoder error\t");
				if (printMotorError)
					printf("Motor error\t");
				if (printOdriveState)
					printf("Odrive state\t");
				if (printOdriveError)
					printf("Odrive error\t");
				printf("\033[0m");
				printf("\n");


				for(i = 1; i <= 10000; i++) {
				ec_send_processdata();
				wkc = ec_receive_processdata(EC_TIMEOUTRET);

				sleep(0.020);

				

				for (int i = 0; i < num_selected_joints; i++) {

					if (!keepRunning)
						return;

					int joint_index = selected_joints[i];
					InputFrame *inp = malloc(sizeof(InputFrame));
					int slave_index = joint_slave_indices[joint_index];
					memcpy(inp, ec_slave[slave_index].inputs, ec_slave[slave_index].Ibytes);

					printf("%s\t", joints[joint_index]);

					switch (joint_index) {
						case 2:
						case 3:
						case 6:
						case 7:
							// Rotational joints (axis 0)
							if (printAbsolutePosition)
								printf("%d\t\t", inp->Axis0AbsolutePosition);
							if (printRelativePosition)
								printf("%d\t\t", inp->Axis0ShadowCount);
							if (printDieBoSlaveError)
								printf("%d\t\t", inp->Axis0DieBoError);
							if (printEncoderError)
								printf("%d\t\t", inp->Axis0EncoderError);
							if (printMotorError)
								printf("%d\t\t", inp->Axis0MotorError);
							if (printOdriveState)
								printf("%d\t\t", inp->Axis0State);
							if (printOdriveError)
								printf("%d\t\t", inp->ODriveError);
							break;

						default:
							//Linear joints (axis 1)
							if (printAbsolutePosition)
								printf("%d\t\t", inp->Axis1AbsolutePosition);
							if (printRelativePosition)
								printf("%d\t\t", inp->Axis1ShadowCount);
							if (printDieBoSlaveError)
								printf("%d\t\t", inp->Axis1DieBoError);
							if (printEncoderError)
								printf("%d\t\t", inp->Axis1EncoderError);
							if (printMotorError)
								printf("%d\t\t", inp->Axis1MotorError);
							if (printOdriveState)
								printf("%d\t\t", inp->Axis1State);
							if (printOdriveError)
								printf("%d\t\t", inp->ODriveError);
							break;
					}

					printf("\n");

					needlf = TRUE;
					osal_usleep(5000);
					free(inp);

				}


				for (int i = 0; i < num_selected_joints; i++) {
					printf("\033[1A"); // move cursor one line up
					// printf("\033[K");  // delete till end of line
				}

				inOP = FALSE;

				}
			} else {

				printf("Not all slaves reached operational state.\n");
				ec_readstate();

				for (i = 1; i<=ec_slavecount ; i++) {
					if (ec_slave[i].state != EC_STATE_OPERATIONAL) {
						printf("Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
							i, ec_slave[i].state, ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
					}
				}

			}

			printf("\nRequest init state for all slaves\n");
			ec_slave[0].state = EC_STATE_INIT;
			/* request INIT state for all slaves */
			ec_writestate(0);

		} else {
			printf("No slaves found!\n");
		}

		printf("End simple test, close socket\n");
		/* stop SOEM, close socket */
		ec_close();

    } else {
        printf("No socket connection on %s\nExecute as root\n",ifname);
    }
}

boolean equals(const char* str1, const char* str2) {
	return ((strcmp(str1, str2) == 0));
}

void signalHandler() {
	for (int i = 0; i < num_selected_joints + 1; i++)
		printf("\n");
	printf("\033[?25h");
	keepRunning = FALSE;
}

int main(int argc, char *argv[]) {

	signal(SIGINT, signalHandler);

	boolean print_help = FALSE;
	for (int i = 1; i < argc; i++)
		print_help |= equals(argv[i], "-h");
   
	if (print_help || argc == 1) {

		ec_adaptert * adapter = NULL;
		printf("Usage: simple_test ifname value jointname\n\n");
		printf("For example: simple_test enp2s0f0 --abs-pos --diebo-err left_hip_fe right_ankle\n");

		printf ("\nAvailable ifname adapters:\n");
		adapter = ec_find_adapters ();
		while (adapter != NULL) {
			printf ("    %s\n", adapter->name);
			adapter = adapter->next;
		}
		ec_free_adapters(adapter);

		printf("\nAvailable joints:\n");
		printf("    left_ankle\n");
		printf("    left_hip_aa\n");
		printf("    left_hip_fe\n");
		printf("    left_knee\n");
		printf("    right_ankle\n");
		printf("    right_hip_aa\n");
		printf("    right_hip_fe\n");
		printf("    right_knee\n");

		printf("\nAvailable values to display:\n");
		printf("    -p, --abs-pos\t(absolute position)\n");
		printf("    -r, --rel-pos\t(relative position)\n");
		printf("    -d, --diebo-err\t(DieBoSlave error)\n");
		printf("    -e, --encoder-err\t(Encoder error)\n");
		printf("    -m, --motor-err\t(Motor error)\n");
		printf("    -s, --state\t\t(Odrive state)\n");
		printf("    -o, --odrive-err\t\t(Odrive error)\n");

		return 0;
	}

	for (int i = 1; i < argc; i++) {

		if (equals(argv[i], "-p") || equals(argv[i], "--abs-pos"))
			printAbsolutePosition = TRUE;
		else if (equals(argv[i], "-r") || equals(argv[i], "--rel-pos"))
			printRelativePosition = TRUE;
		else if (equals(argv[i], "-d") || equals(argv[i], "--diebo-err"))
			printDieBoSlaveError = TRUE;
		else if (equals(argv[i], "-e") || equals(argv[i], "--encoder-err"))
			printEncoderError = TRUE;
		else if (equals(argv[i], "-m") || equals(argv[i], "--motor-err"))
			printMotorError = TRUE;
		else if (equals(argv[i], "-s") || equals(argv[i], "--state"))
			printOdriveState = TRUE;
		else if (equals(argv[i], "-o") || equals(argv[i], "--odrive-err"))
			printOdriveError = TRUE;
		else
			continue;

		num_values_to_print++;
	}

	if (num_values_to_print == 0) {
		printAbsolutePosition = TRUE;
		printRelativePosition = TRUE;
		printDieBoSlaveError = TRUE;
		printEncoderError = TRUE;
		printMotorError = TRUE;
		printOdriveState = TRUE;
		printOdriveError = TRUE;
	}

	for (int i = 1; i < argc; i++) {
		for (int j = 0; j < 8; j++) {
			if (equals(argv[i], joints[j])) {
				selected_joints[num_selected_joints] = j;	
				num_selected_joints++;
				break;
			}
		}
	}

	if (num_selected_joints == 0) {
		num_selected_joints = 8;
		for (int i = 0; i < num_selected_joints; i++)
			selected_joints[i] = i;
	}

	if (num_selected_joints == 1) {
		printf("\nPress Enter to save the current line to the terminal.");
	}

	simpletest(argv[1]);

   return 0;
}
